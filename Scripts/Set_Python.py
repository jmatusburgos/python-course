

ids = set(range(10))

males = set([1,3,5,6,7])

females = ids - males

#print(ids)
#print(males)
#print(females)

everyone = males | females

#print(everyone)

everyone & set([1,2,3])
#print("Everyone &")
#print(everyone)

# antidisestablishmentarianism

word = "antidisestablishmentarianism"

letters = set(word) #Set returns only distinct letters


#Dictionaries

age = {} # <class 'dict'>

age = dict() # <class 'dict'>

#age = {"Tim": 29 , "Jim": 31, "Pam":27, "Sam":35}

age = dict(Tim= 29 , Jim= 31, Pam=27, Sam=35)

age["Tom"] = 50
names = age.keys()


age["Nick"] = 31

print("Tim" in age)






