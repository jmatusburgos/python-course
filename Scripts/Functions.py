


def add(a,b):
    mysum = a + b
    return mysum

print(add(12,15))


#---------------------------------------------------

def add_and_sub(a,b):
    mysum = a+b
    mydiff = a - b
    return (mysum,mydiff)
print(add_and_sub(20,15)) 

#---------------------------------------------------

def modify(myList):
    myList[0] *= 10

L = [1,3,5,7,9]
modify(L)

print(L)

# ----------- simple functions  --------------------
def intersect(s1,s2):
    res = []
    for x in s1:
        if x in s2:
            res.append(x)
    return res      

intersect([1,2,3,4,5], [3,4,5,6,7])

import random
def password(length):
    pw = str()
    characters = "abcdefghijklmnopqrstuvwxyz" + "0123456789"
    for i in range(length):
        pw += random.choice(characters)
    return pw

password(50)

