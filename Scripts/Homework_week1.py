#---- Exercise 1a
import string
alphabet = string.ascii_letters
print(alphabet)

#---- Exercise 1b
sentence = 'Jim quickly realized that the beautiful gowns are expensive'
sentence = sentence.replace(" ","")

count_letters = {}

alphabet = string.ascii_letters

for letter in sentence:
    if not letter in count_letters:
        
        count_letters[letter] = 1
    else :
        count_letters[letter] += 1

print(count_letters)
            
#---- Exercise 1c
sentence = 'Jim quickly realized that the beautiful gowns are expensive'

# Create your function here!
def counter(input_string):
    input_string = input_string.replace(" ","")
    count_letters = {}
    alphabet = string.ascii_letters
    for letter in input_string:
        if not letter in count_letters:
            count_letters[letter] = 1
        else :
            count_letters[letter] += 1
    return count_letters

#counter(sentence)    

#---- Exercise 1c

address = "Four score and seven years ago our fathers brought forth Now we are engaged in a great civil war  testing whether that nation  or any nation so conceived                                   and so dedicated         \n\n                                                                                                                                upon this continent  \\\nNow we are engaged in a great civil war  testing whether that nation  or any nation so conceived   can long endure. We are met on a great battle...   \n\n                                                                                                                                       a new nation  \\\nNow we are engaged in a great civil war  testing whether that nation  or any nation so conceived   as a final resting place for those who died here   \n\n                                                                                                                      conceived in liberty  \\\nNow we are engaged in a great civil war  testing whether that nation  or any nation so conceived   that the nation might live. This we may   \n\n                                                                                                  and dedicated to the proposition that all men are created equal.  \nNow we are engaged in a great civil war  testing whether that nation  or any nation so conceived in all propriety do."

address_count = counter(address)

maxValue = sorted(address_count.values())[-1]
#most_frequent_letter = ""

for letter in address_count:
    if(address_count[letter] == maxValue):
        most_frequent_letter = letter
        break

print(most_frequent_letter)


#---- Exercise 2a
import math
print(math.pi/4)

#-------- Exercise 2b
import random

print (random.uniform(-1,1))

#-------- Exercise 2c
import math

def distance(x, y):
    return (((x[1]- x[0]) ** 2) + ((y[1] - y[0]) ** 2))** 2

x=(0,0)
y=(1,1)  

print(distance(x,y))

#---------------
import math

def distance(x, y):
    """
        Given x and y, find their distance.
        This is given by sqrt(sum((x-y)**2)).
    """
    if len(x) != len(y):
        return "x and y do not have the same length!"
    else:
        square_differences = [(x[i]-y[i])**2 for i in range(len(x))]
        print(square_differences)
        return math.sqrt(sum(square_differences))

print(distance((0,0),(1,1)))   
   