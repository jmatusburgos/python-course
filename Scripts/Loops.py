#for x in range(10):
    #print(x)

names = ["Jim", "Tom","Nick","Pam","Sam","Tim"]

#for name in names:
    #print(name)

#for i in range(len(names)): #Best way for loop declare
#    print(names[i])
  
age = dict(Tim= 31,Nick= 31, Jim= 31, Pam=27, Sam=35, Tom=50)

# for name in age:
#     print(name,age[name])

for name in sorted(age.keys(),reverse=True):
    print(name,age[name])

